use std::fs;

use erased_serde::Serialize;
use futures::IntoFuture;
use futures::Stream;
use telebot::RcBot;
use tokio_core::reactor::Core;

use telebot::functions::*;
use telebot::objects::{input_message_content::Text, InlineQueryResultArticle as IQRA};

fn main() {
    println!("Starting bot...");
    let mut lp = Core::new().unwrap();

    let token = fs::read_to_string("token.txt")
        .expect("Could not read token!")
        .trim()
        .to_owned();
    let bot = RcBot::new(lp.handle(), &token).update_interval(200);

    loop {
        println!("Setting up stream...");
        let stream = bot
            .get_stream()
            .filter_map(|(bot, msg)| msg.inline_query.map(|query| (bot, query)))
            .and_then(|(bot, query)| {
                let result: Vec<Box<Serialize>> = vec![Box::new(IQRA::new(
                    "Klingt lustig".into(),
                    Box::new(Text::new("Klingt lustig".into())),
                ))];
                bot.answer_inline_query(query.id, result)
                    .is_personal(true)
                    .send()
            });

        println!("Starting event loop...");
        if let Err(e) = lp.run(stream.for_each(|_| Ok(())).into_future()) {
            eprintln!("Event loop shutdown:");
            for (i, cause) in e.iter_causes().enumerate() {
                eprintln!(" => {}: {}", i, cause);
            }
        }
    }
}
